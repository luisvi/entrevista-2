# Entrevista 2 (Atra....)

# Paint

Tienes que hacer una aplicacion parecida al famoso paint de Microsoft ;). Esta aplicación
deberá tener solo las siguientes funcionalidades:

- Deshacer/rehacer
- Selección de color
- Grueso de línea

Se adjunta un wireframe orientativo de como podrías hacerlo. Puedes modificar el UX como
creas conveniente.

Puedes usar cualquier framework, librería, etc. También puedes hacerlo en plain ol’ JS ;)


# Consideraciones

Debes proponer la arquitectura a utilizar teniendo en cuenta que el proyecto debería
poderse distribuir como app multidispositivo en un futuro próximo.
La aplicación va a tener un ritmo rápido de iteraciones, incorporando en las próximas
semanas nuevas funcionalidades como filtros, etc.



![](wireframe-paint.png)